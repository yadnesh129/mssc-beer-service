package micro.mssc.beerservice.bootstrap;

import micro.mssc.beerservice.domain.Beer;
import micro.mssc.beerservice.domain.BeerRepository;
import micro.mssc.beerservice.web.model.v2.BeerStyleEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class BeerLoader implements CommandLineRunner {

  @Autowired
  private BeerRepository beerRepository;
  @Override
  public void run(String... args) throws Exception {
    loadBeerObjects();
  }

  private void loadBeerObjects() {
    if (beerRepository.count() == 0) {

      beerRepository.save(Beer.builder()
              .beerName("Mango")
              .beerStyle(BeerStyleEnum.IPA)
              .quantityToBrew(200)
              .upc(1698487799L)
              .price(new BigDecimal("13.95"))
              .build());

      beerRepository.save(Beer.builder()
              .beerName("Apple")
              .beerStyle(BeerStyleEnum.IPA)
              .quantityToBrew(300)
              .upc(1498487799L)
              .price(new BigDecimal("12.95"))
              .build());
    }

    System.out.println("Loaded Beers : " + beerRepository.count());
  }
}
