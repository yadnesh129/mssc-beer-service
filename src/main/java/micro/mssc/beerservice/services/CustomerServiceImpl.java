package micro.mssc.beerservice.services;


import lombok.extern.slf4j.Slf4j;
import micro.mssc.beerservice.web.model.CustomerDTO;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {
  @Override
  public CustomerDTO getById(UUID id) {
    return CustomerDTO.builder().id(id)
            .name("Test1")
            .build();
  }

  @Override
  public CustomerDTO saveNewCustomer(CustomerDTO customerDTO) {
    return CustomerDTO.builder().id(customerDTO.getId()).build();
  }

  @Override
  public void updateCustomer(UUID customerId, CustomerDTO customerDTO) {

  }

  @Override
  public void deleteCustomer(UUID customerId) {
    log.debug("Deleting customer ..... {}", customerId);
  }
}
