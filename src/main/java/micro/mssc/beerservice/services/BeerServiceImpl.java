package micro.mssc.beerservice.services;

import lombok.extern.slf4j.Slf4j;
import micro.mssc.beerservice.web.model.BeerDto;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by jt on 2019-04-20.
 */
@Slf4j
@Service
public class BeerServiceImpl implements BeerService {
  @Override
  public BeerDto getBeerById(UUID beerId) {
    return BeerDto.builder().id(UUID.randomUUID())
            .beerName("Galaxy Cat")
            .beerStyle("Pale Ale")
            .build();
  }

  @Override
  public BeerDto saveNewBeer(BeerDto beerDto) {
    return BeerDto.builder().id(beerDto.getId()).build();
  }

  @Override
  public void updateBeer(UUID beerId, BeerDto beerDto) {

  }

  @Override
  public void deleteBeer(UUID beerId) {
    log.debug("Deleting a beer ...{}", beerId);

  }
}
