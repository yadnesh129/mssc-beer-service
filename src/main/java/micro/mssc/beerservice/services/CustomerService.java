package micro.mssc.beerservice.services;

import micro.mssc.beerservice.web.model.CustomerDTO;

import java.util.UUID;

public interface CustomerService {
  CustomerDTO getById(UUID id);

  CustomerDTO saveNewCustomer(CustomerDTO customerDTO);

  void updateCustomer(UUID customerId, CustomerDTO customerDTO);

  void deleteCustomer(UUID customerId);
}
