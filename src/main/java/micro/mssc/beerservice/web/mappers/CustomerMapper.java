package micro.mssc.beerservice.web.mappers;


import micro.mssc.beerservice.domain.Customer;
import micro.mssc.beerservice.web.model.CustomerDTO;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerMapper {
  Customer customerDtoToCustomer(CustomerDTO customerDTO);
  CustomerDTO customerToCustomerDto(Customer customer);
}
