package micro.mssc.beerservice.web.mappers;

import micro.mssc.beerservice.domain.Beer;
import micro.mssc.beerservice.web.model.v2.BeerDtoV2;
import org.mapstruct.Mapper;

@Mapper(uses = {DateMapper.class})
public interface BeerMapper {
  BeerDtoV2 beerToBeerDto(Beer beer);
  Beer beerDtoToBeer(BeerDtoV2 beerDto);
}
