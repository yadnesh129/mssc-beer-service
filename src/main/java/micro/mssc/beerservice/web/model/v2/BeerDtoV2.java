package micro.mssc.beerservice.web.model.v2;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class BeerDtoV2 {
  @Null
  private UUID id;
  @Null
  private Integer version;
  @Null
  private OffsetDateTime createdDate;
  @Null
  private OffsetDateTime lastModifiedDate;
  @NotBlank
  private String beerName;
  @NotNull
  private BeerStyleEnum beerStyle;
  @Positive
  @NotNull
  private Long upc;
  private BigDecimal price;
  private Integer quantity;
}
