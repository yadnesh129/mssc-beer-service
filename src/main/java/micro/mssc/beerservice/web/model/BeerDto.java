package micro.mssc.beerservice.web.model;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * Created by jt on 2019-04-20.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BeerDto {

  @Null
  private UUID id;
  @NotBlank
  @Size(min=3,max = 100)
  private String beerName;
  @NotBlank
  @NotNull
  private String beerStyle;
  @Positive
  @NotNull
  private Long upc;
}
