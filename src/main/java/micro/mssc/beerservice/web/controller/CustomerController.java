package micro.mssc.beerservice.web.controller;


import jakarta.validation.Valid;
import micro.mssc.beerservice.services.CustomerService;
import micro.mssc.beerservice.web.model.CustomerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.UUID;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
  @Autowired
  private CustomerService customerService;

  @GetMapping({"/{customerId}"})
  public ResponseEntity<CustomerDTO> getCustomer(@PathVariable("customerId") UUID id) {
    return new ResponseEntity<CustomerDTO>(customerService.getById(id), HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity handlePost(@Valid @RequestBody CustomerDTO customerDTO) {
    customerDTO = customerService.saveNewCustomer(customerDTO);

    HttpHeaders headers = new HttpHeaders();
    headers.add("Location", "https:localhost:8080/api/v1/beer/" + customerDTO.getId().toString());

    return new ResponseEntity<>(headers, HttpStatus.CREATED);
  }

  @PutMapping({"/customerId"})
  public ResponseEntity handleUpdate(@PathVariable("customerId") UUID customerId, @Valid @RequestBody CustomerDTO customerDTO) {
    customerService.updateCustomer(customerId, customerDTO);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }

  @DeleteMapping({"/customerId"})
  public void deleteCustomer(@PathVariable("customerId") UUID customerId) {
    customerService.deleteCustomer(customerId);
  }


}
