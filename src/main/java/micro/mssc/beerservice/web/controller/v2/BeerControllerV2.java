package micro.mssc.beerservice.web.controller.v2;


import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import micro.mssc.beerservice.services.BeerServiceV2;
import micro.mssc.beerservice.web.model.v2.BeerDtoV2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;



import java.util.UUID;

@Slf4j
@Validated//perform validation on method input paramters
@RequiredArgsConstructor
@RestController
@RequestMapping("api/v2/beer")
public class BeerControllerV2 {
  private final BeerServiceV2 beerServiceV2;

  @GetMapping({"/{beerId}"})
  public ResponseEntity<BeerDtoV2> getBeer(@NotNull @PathVariable("beerId") UUID beerId) {

    return new ResponseEntity<>(beerServiceV2.getBeerById(beerId), HttpStatus.OK);
  }

  @PostMapping//post create new beer
  public ResponseEntity handlePost(@NotNull @Valid @RequestBody BeerDtoV2 beerDto) {
    log.debug("in handle post...");

    val savedDTO = beerServiceV2.saveNewBeer(beerDto);
    val headers = new HttpHeaders();
    headers.add("Location", "https:localhost:8080/api/v1/beer/" + savedDTO.getId().toString());

    return new ResponseEntity(headers, HttpStatus.CREATED);
  }

  @PutMapping({"/beerId"})
  public ResponseEntity handleUpdate(@PathVariable("beerId") UUID beerId, @Valid @RequestBody BeerDtoV2 beerDto) {
    beerServiceV2.updateBeer(beerId, beerDto);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }

  @DeleteMapping({"/beerId"})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteBeer(@PathVariable("beerId") UUID beerId) {
    beerServiceV2.deleteBeer(beerId);
  }

}
