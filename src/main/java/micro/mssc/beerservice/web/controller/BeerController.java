package micro.mssc.beerservice.web.controller;


import micro.mssc.beerservice.services.BeerService;
import micro.mssc.beerservice.web.model.BeerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * Created by jt on 2019-04-20.
 */
@Deprecated
@RequestMapping("/api/v1/beer")
@RestController
public class BeerController {
  @Autowired
  private  BeerService beerService;

  @GetMapping({"/{beerId}"})
  public ResponseEntity<BeerDto> getBeer(@PathVariable("beerId") UUID beerId) {

    return new ResponseEntity<>(beerService.getBeerById(beerId), HttpStatus.OK);
  }

  @PostMapping//post create new beer
  public ResponseEntity handlePost(@Validated @RequestBody BeerDto beerDto) {
    BeerDto savedDTO = beerService.saveNewBeer(beerDto);
    HttpHeaders headers = new HttpHeaders();
    headers.add("Location", "https:localhost:8080/api/v1/beer/");

    return new ResponseEntity(headers, HttpStatus.CREATED);
  }

  @PutMapping({"/beerId"})
  public ResponseEntity handleUpdate(@PathVariable("beerId") UUID beerId, @Validated @RequestBody BeerDto beerDto) {
    beerService.updateBeer(beerId, beerDto);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }

  @DeleteMapping({"/beerId"})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteBeer(@PathVariable("beerId") UUID beerId) {
    beerService.deleteBeer(beerId);

  }

}
