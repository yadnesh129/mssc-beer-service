package micro.mssc.beerservice.domain;

import jakarta.persistence.*;
import lombok.*;
import micro.mssc.beerservice.web.model.v2.BeerStyleEnum;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Beer {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  @Column(length = 36, columnDefinition = "varchar", updatable = false, nullable = false)
  private UUID id;
  @Version
  private Long version;
  @CreationTimestamp
  @Column(updatable = false)
  private Timestamp createdDate;
  @UpdateTimestamp
  private Timestamp lastModifiedDate;
  private String beerName;
  private BeerStyleEnum beerStyle;
  @Column(unique = true)
  private Long upc;
  private BigDecimal price;
  private Integer minOnHand;
  private Integer quantityToBrew;


}
