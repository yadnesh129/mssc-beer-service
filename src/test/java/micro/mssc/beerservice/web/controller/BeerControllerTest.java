package micro.mssc.beerservice.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import micro.mssc.beerservice.domain.Beer;
import micro.mssc.beerservice.domain.BeerRepository;
import micro.mssc.beerservice.web.model.BeerDto;
import micro.mssc.beerservice.web.model.v2.BeerStyleEnum;
import org.hibernate.validator.internal.metadata.raw.ConstrainedField;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.util.StringUtils;

import org.springframework.restdocs.constraints.ConstraintDescriptions;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.restdocs.snippet.Attributes.key;
@AutoConfigureRestDocs(uriScheme = "https",uriHost = "dev.springframework.guru",uriPort = 80)
@ExtendWith(RestDocumentationExtension.class)
@WebMvcTest(BeerController.class)
@ComponentScan(basePackages = "micro.mssc.beerservice")
public class BeerControllerTest {

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;
  @MockBean
  private BeerRepository beerRepository;

  @Test
  void getBeerById() throws Exception {
    given(beerRepository.findById(any())).willReturn(Optional.of(Beer.builder().build()));

    mockMvc.perform(get("/api/v1/beer/{beerId}", UUID.randomUUID().toString())
//                    .param('iscold','yes')
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andDo(document("v1/beer-get",
                    pathParameters(
                      parameterWithName("beerId").description("UUID of desired beer to get")
                    )
            ,responseFields(
                    fieldWithPath("id").description("Id of beer"),
                            fieldWithPath("beerName").description("Name of beer"),
                            fieldWithPath("beerStyle").description("Beer Style"),
                            fieldWithPath("upc").description("upc")

                    )));
  }

  @Test
  void saveNewBeer() throws Exception {
    BeerDto beerDto =  getValidBeerDto();
    String beerDtoJson = objectMapper.writeValueAsString(beerDto);

    ConstrainedFields fields=new ConstrainedFields(BeerDto.class);

    mockMvc.perform(post("/api/v1/beer")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(beerDtoJson))
            .andExpect(status().isCreated())
            .andDo(document("v1/beer-new",
                    requestFields(
                            fields.withPath("id").ignored(),
                            fields.withPath("beerName").description("Name of the beer"),
                            fields.withPath("beerStyle").description("Beer Style"),
                            fields.withPath("upc").description("upc")
                    )));
  }

  BeerDto getValidBeerDto(){
    return BeerDto.builder()
            .beerName("Nice Ale")
            .beerStyle(String.valueOf(BeerStyleEnum.ALE))
            .upc(123123123123L)
            .build();

  }

  private static class ConstrainedFields{
    private final ConstraintDescriptions constraintDescriptions;

    ConstrainedFields(Class<?> input){
      this.constraintDescriptions=new ConstraintDescriptions(input);

    }

    private FieldDescriptor withPath(String path) {
      return fieldWithPath(path).attributes(key("constraints").value(StringUtils
              .collectionToDelimitedString(this.constraintDescriptions
                      .descriptionsForProperty(path), ". ")));
    }
  }

}
